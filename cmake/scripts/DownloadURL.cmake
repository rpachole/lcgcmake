# - Download a [compressed] file from the web when the standard URL option does not
#   work on the ExternalProject
#   Parameters: url  - url of the file
#               source_dir - source directory
#               timeout - timeout in seconds
#               md5  - expected md5
#               nodecompress - flag to skip decompression
#               quiet - flag to avoid verbosity
#

if(timeout)
  set(timeout_args TIMEOUT ${timeout})
  set(timeout_msg "${timeout} seconds")
else()
  set(timeout_msg "none")
endif()
if(md5)
  set(md5_args EXPECTED_MD5 ${md5})
else()
  set(md5_msg "none")
endif()


if("${url}" MATCHES "^[a-z]+://")
  string(REGEX MATCH "[^/\\?]*$" fname "${url}")
  string(REPLACE ";" "-" fname "${fname}")
  set(file ${source_dir}/${fname})

  #---Downloading-------------------------------------------------------------------------------------
  if(NOT quiet)
    message(STATUS "downloading...
    src='${url}'
    dst='${file}'
    timeout='${timeout_msg}'
    md5='${md5_msg}'")
  endif()

  file(DOWNLOAD "${url}" "${file}" ${md5_args} ${timeout_args} STATUS status LOG log)

  list(GET status 0 status_code)
  list(GET status 1 status_string)
  if(NOT status_code EQUAL 0)
    message(FATAL_ERROR "error: downloading '${remote}' failed
    status_code: ${status_code}
    status_string: ${status_string}
    log: ${log}")
  endif()
  
  if(NOT quiet)
    message(STATUS "downloading... done")
  endif()

  #---extract file------------------------------------------------------------------------------------
  if(NOT nodecompress)
    if(file MATCHES "(\\.|=)(gz|tgz|zip)$")
      get_filename_component(filename "${file}" ABSOLUTE)
      if(NOT EXISTS "${filename}")
        message(FATAL_ERROR "error: file to decompress does not exist: '${filename}'")
      endif()
      message(STATUS "decompressing... src='${filename}'")
      execute_process(COMMAND gzip -df  ${filename} RESULT_VARIABLE rv)
      if(NOT rv EQUAL 0)
        message(FATAL_ERROR "error: decompressing of '${filename}' failed")
      endif()
    else()
      message(STATUS "file is already de-compressed")
    endif()
  endif()
endif()
