#!/usr/bin/env python

import os
import shutil
import sys
import time
import logging
import argparse

class InstallBundle(object):
    def __init__(self, prefix, blacklist=[], greylist=[]):
        self.prefix = prefix
        self.blacklist = blacklist
        self.greylist = greylist
        self.topdir_whitelist = ['aclocal', 'cmake', 'emacs', 'fonts', 'include', 'macros', 'test', 'tests',
                                 'bin', 'config', 'etc', 'icons', 'lib', 'lib64', 'man', 'tutorials', 'share', 'src']
        self.concatenatelist = ['easy-install.pth']
        
    def splitall(self, path):
        allparts = []
        while 1:
            parts = os.path.split(path)
            if parts[0] == path:  # sentinel for absolute paths
                allparts.insert(0, parts[0])
                break
            elif parts[1] == path:  # sentinel for relative paths
                allparts.insert(0, parts[1])
                break
            else:
                path = parts[0]
                allparts.insert(0, parts[1])
        return allparts

    def copy_pkg(self, pkg_root):
    
        logging.info('Package root = {0}'.format(pkg_root))

        for (dir_path, dirnames, filenames) in os.walk(pkg_root, followlinks=False):

            dirpath = dir_path.replace(pkg_root, '.')
            dirpath_s = self.splitall(dirpath)

            # Elinimate any top level file or directory not in the topdir_whitelist
            if len(dirpath_s) == 1 or dirpath_s[1] not in self.topdir_whitelist:
                continue
                
            view_dir = os.path.realpath(os.path.join(self.prefix, dirpath))

            if not os.path.exists(view_dir):
                try:
                    os.makedirs(view_dir)
                except OSError as e:
                    if e.errno == 20:
                        logging.warning("Target already exists and is file: {0}".format(view_dir))
                        logging.info("Added from: {0}".format(os.path.realpath(view_dir)))
                        logging.info("Conflicts with: {0}".format(os.path.realpath(dir_path)))
                    else:
                        raise e

            for f in filenames:
                if f in self.blacklist or f.startswith('.') or f.endswith('-env.sh') or f.endswith('~'):
                    continue

                source = os.path.join(dir_path, f)
                target = os.path.join(view_dir, f)

                if f in self.concatenatelist:
                    open(target, 'a').write(open(source, 'r').read())
                    continue

                if not os.path.exists(target):
                    try:
                        shutil.copy(source, target)
                    except OSError as e:
                        if e.errno == 20:
                            logging.warning("Target already exisits and is file: {0}".format(f))
                        else:
                            raise e
                else:
                    if f not in self.greylist:
                        logging.warning("File already exists: {0}".format(target))
                        
def main():
    helpstring = """{0} [options] <install-prefix> <package-root>... 

This package bundles a number of packages to single  <install-prefix>.
"""

    parser = argparse.ArgumentParser(usage=helpstring.format(sys.argv[0]))
    parser.add_argument('prefix', metavar='prefix', nargs=1)
    parser.add_argument('packages', metavar='packages', nargs='+')
    parser.add_argument('--loglevel', choices=['ERROR', 'WARNING', 'INFO', 'DEBUG'], action='store',
                       dest='loglvl', default='INFO', help='Set logging level (default: INFO)')

    args = parser.parse_args()

    logging.basicConfig(format=u'*** %(levelname)s: %(message)s', level=args.loglvl)
 
    blacklist = ['version.txt', '.filelist', 'README', 'LICENSE', 'INSTALL']
    greylist = ['site.py', 'site.pyc', 'easy_install', 'easy_install-2.7', 'setuptools.pth']
 
    bundle = InstallBundle(prefix=args.prefix[0], blacklist=blacklist, greylist=greylist)

    for p in args.packages:
        if os.path.isfile(p) :
            for q in open(p).read().split(';'):
                bundle.copy_pkg(q)
        elif os.path.isdir(p) :
          bundle.copy_pkg(p)

if __name__ == '__main__':
    exit(main())

