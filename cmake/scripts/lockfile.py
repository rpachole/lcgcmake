#!/usr/bin/env python
"""
Program to emulate the lockfile command
<pere.mato@cern.ch>
Version 1.0
"""

from __future__ import print_function
import os, sys, time, socket, errno, shutil
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-v', '--version', action='version', version='1.0', help='display the version number and exit')
parser.add_argument('-l', default='0', type=int,  dest='timeout',  help='set locktimeout to TIMEOUT seconds')
parser.add_argument('-r', default='0', type=int,  dest='retries', help='set number of retries to get the lock')
parser.add_argument('-u', '--unlock', action='store_true', help='Release the lock by deleting the lockfile')
parser.add_argument('file', help='lockfile name') 
args = parser.parse_args()

#---Timeout and wait times----------------------------------------------------------------------
timeout = args.timeout
end_time = time.time()
if timeout > 0: end_time += timeout
retries = args.retries
lock_file = args.file

if timeout == 0:
  wait = 0.1
else:
  wait = max(0, timeout/10)

#---Unique name---------------------------------------------------------------------------------
hostname = socket.gethostname()
pid = os.getpid()
unique_name = os.path.join(lock_file,"%s.%s" % (hostname, pid))

#---Unlocking-----------------------------------------------------------------------------------
if args.unlock :
  try:
    shutil.rmtree(lock_file)
  except OSError as e:
    print("Error removing lock for '%s' (%s)" % (lock_file, e.strerror), file=sys.stderr )
    exit(1)
  else:
    exit(0)
 
 
#---Try to create a directory-------------------------------------------------------------------
ntries = 0
while True:
  try:
    os.mkdir(lock_file)
  except OSError as e:
    if e.errno == errno.EEXIST:
       # Already locked.
       if os.path.exists(unique_name):
       # Already locked by me.
         exit(0)
       if timeout > 0 and time.time() > end_time:
         print("Timeout waiting to acquire" " lock for '%s'" % lock_file, file=sys.stderr)
         exit(1)
       elif retries > 0 and ntries > retries :
         print("Exceeded number of %d retries to acquire" " lock for '%s'" % (retries, lock_file), file=sys.stderr)
         exit(1)
       else:
         time.sleep(wait)
         ntries += 1
    else:
      # Couldn't create the lock for some other reason
      print("failed to create '%s' (%s)" % (lock_file, e.strerror), file=sys.stderr)
      exit(1)
  else:
    open(unique_name, "wb").close()
    exit(0)

