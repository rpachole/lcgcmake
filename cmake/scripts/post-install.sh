#!/bin/sh

# set env variable NIGHTLY_MODE to activate nightly mode

export LANG=C
listname=".filelist"

# === Replace install paths in files from filelist
THIS="$0"
PREFIX="$(/bin/sh -c "dirname $THIS")"

# Use INSTALLDIR env variable or automatic RPM_INSTALL_DIR as target prefix
while [ ! -d "$INSTALLDIR" ]; do
  [ ! -z "$RPM_INSTALL_PREFIX" ] && INSTALLDIR="$RPM_INSTALL_PREFIX" || read -e -p "Type new install directory : " INSTALLDIR
done

# extract old install dir from .filelist (should be first line)
OLDINSTALLDIR="$(head -1 $PREFIX/$listname)"
sed -i -e '1d' $PREFIX/$listname  # delete first line
echo "Replacing $OLDINSTALLDIR -> $INSTALLDIR"

# prepare package list for replacing

# loop over files (lines without '->')
cat $PREFIX/$listname | grep -v -- '->' | while read name; do
  test "$name" == ".filelist" && continue  # skip .filelist
  echo "=== Patching $name ... ==="
  #old=$(mktemp -t lcg.XXXXX)
  #cp -f "$PREFIX/$name" "$old"
  # loop over all target paths (after '->' delimiter)
  cat $PREFIX/$listname | grep -- '->' | awk -F'->' '{print $2}'| sort | uniq | while read newpath; do
    # loop over all source paths for given target path, use sorting by path length to resolve issues like /var/build/... and /build/...
    # 1 target path is related with multiple (at least 2 -- real and 'usual') source paths  
    cat $PREFIX/$listname | grep -- '->' | grep -- "$newpath" | awk -F'->' '{ print length($1) " " $0; }' | sort -r -n | cut -d ' ' -f 2- | while read line; do
      olddir="$(echo $line | awk -F'->' '{print $1}')"
      if test -z "$NIGHTLY_MODE"; then
        newdir="${INSTALLDIR}/$(echo $line | awk -F'->' '{print $2}')"
      else
        newdir=$(echo $line | awk -F'->' '{print $1}' | sed "s@$OLDINSTALLDIR@$INSTALLDIR@g")
      fi
      # patching
      if [ `uname` = "Linux" ]; then
        test ! -z "$olddir" && test ! -z "$newdir" && sed -i -e "s@$olddir@$newdir@g" -e '/afs/s,/\.cern.ch,/cern.ch,g' "$PREFIX/$name"
      else
        test ! -z "$olddir" && test ! -z "$newdir" && sed -i '' -e "s@$olddir@$newdir@g" -e '/afs/s,/\.cern.ch,/cern.ch,g' "$PREFIX/$name"
      fi
    done
  done
  # show diff
  #diff -u "$old" "$PREFIX/$name"
  #rm -f "$old"
  #echo
done

exit 0
