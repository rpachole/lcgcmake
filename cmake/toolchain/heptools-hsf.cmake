cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools we use
# (must be done before including heptools-common to allow evolution of the
# structure)
set(heptools_version hsf)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# please keep alphabetic order and the structure (tabbing).
# it makes it much easier to edit/read this file!


# Application Area Projects
LCG_AA_project(ROOT  v6.12.06)
LCG_AA_project(Geant4 10.04.p01)
LCG_AA_project(HepMC 2.06.09)

# Other externals for the complete HSF stack 
#LCG_AA_project(DD4hep 00-20)
#LCG_external_package(ponder            2.1.1                                    )
#LCG_external_package(CMake             3.8.2                                    )
#LCG_external_package(CppUnit           1.12.1_p1                 author=1.12.1  )
#LCG_external_package(doxygen           1.8.11                                   ) 
#LCG_external_package(eigen             3.2.9                                    )
#LCG_external_package(HepPDT            2.06.01                                  )
#LCG_external_package(icu4c             61_1                                     )
#LCG_external_package(jsoncpp           1.7.2                                    )
#LCG_external_package(jsonnet           0.10.0                                   )
#LCG_external_package(tiff              4.0.9                                    )
#LCG_external_package(uuid              1.42                                     )
#LCG_external_package(range_v3          0.3.6                                    )
#LCG_external_package(readline          2.5.1p1                                  )


# ROOT Dependencies
LCG_external_package(Python            2.7.14                                   )
LCG_external_package(fftw              3.3.4                     fftw3          )
LCG_external_package(graphviz          2.28.0                                   )
LCG_external_package(GSL               2.4                                      )
LCG_external_package(xrootd            4.8.2                                    )
LCG_external_package(numpy             1.14.2                                   )
LCG_external_package(tbb               2018_U1                                  )
LCG_external_package(blas              3.8.0                                    )
LCG_external_package(zlib              1.2.11                                   )
LCG_external_package(libxml2           2.9.7                                    )
LCG_external_package(cfitsio           3410                                     )
LCG_external_package(Vc                1.3.2                                    )

# Root binary Dependencies
if(${LCG_OS} STREQUAL slc OR ${LCG_OS} STREQUAL centos)
  if (NOT ${LCG_HOST_ARCH} STREQUAL i686)
   LCG_external_package(CASTOR          2.1.13-6               castor            )
   LCG_external_package(dcap            2.47.7-1               Grid/dcap         )
   LCG_external_package(gfal            1.13.0-0               Grid/gfal         )
   LCG_external_package(srm_ifce        1.13.0-0               Grid/srm-ifce     )
  endif()
endif()

# Geant4 Dependencies
LCG_external_package(Qt5               5.9.2                    qt5             )
LCG_external_package(XercesC           3.1.4                    author=3.1.4    )
LCG_external_package(CLHEP             2.4.0.1                  clhep           )
LCG_external_package(VecGeom           v00.05.01                                )
LCG_external_package(expat             2.2.5                                    )

# Boost
LCG_external_package(Boost             1.65.0                                   )

# xerces-c Dependencies
if(${LCG_OS}${LCG_OSVERS} STREQUAL slc6)
  LCG_external_package(curl              7.19.7                                 )
else()
  LCG_external_package(curl              7.59.0                                   )
endif()

# Qt5 Dependencies
if (NOT (${LCG_OS} STREQUAL ubuntu ))
  LCG_external_package(libxkbcommon      0.7.1                                    )
endif()
LCG_external_package(glib              2.52.2                                    )

# numpy Dependencies
LCG_external_package(setuptools        36.0.1                                   )

# graphviz Dependencies
LCG_external_package(libtool           2.4.2                                    )
LCG_external_package(pango             1.40.13                                  )
LCG_external_package(fontconfig        2.12.6                                   )

# Python Dependencies
LCG_external_package(sqlite            3210000                                  )
if(${LCG_OS} MATCHES mac)
    LCG_external_package(openssl           1.0.2d                                   )
endif()

# glib Dependencies
LCG_external_package(libffi            3.2.1                                )
LCG_external_package(pcre              8.38                                     )
LCG_external_package(pkg_config        0.28                                     )
LCG_external_package(gettext           0.19.8.1                                 )

# pango Dependencies
LCG_external_package(cairo             1.15.8                                   )
LCG_external_package(harfbuzz          1.6.3                                    )
LCG_external_package(freetype          2.6.3                                    )

# fontconfig Dependencies
LCG_external_package(gperf             3.1                                      )

# cairo Dependencies
LCG_external_package(png               1.6.17                                   )
LCG_external_package(pixman            0.34.0                                    )

# R Dependencies
LCG_external_package(zeromq            4.1.6                                    )

# VecGeom Dependencies
LCG_external_package(veccore           0.4.2                                      )
LCG_external_package(umesimd           0.8.1                                     )

# MonteCarlo Generators
LCG_external_package(pythia6           429.2          ${MCGENPATH}/pythia6    author=6.4.28 hepevt=10000  )

# Prepare the search paths according to the versions above
LCG_prepare_paths()
