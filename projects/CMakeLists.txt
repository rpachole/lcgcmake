#--- General parameters ----------------------------------------------------------------------------
set(Python_cmd ${Python_home}/bin/${PYTHON})
set(PySetupOptions --root=/ --prefix=<INSTALL_DIR>)

#---setuptools------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  setuptools
  URL ${GenURL}/setuptools-${setuptools_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND ${PYTHON} bootstrap.py
          COMMAND ${PYTHON} setup.py install ${PySetupOptions}
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---numpy---------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  numpy
  URL ${GenURL}/numpy-${numpy_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND ${PYTHON} setup.py install ${PySetupOptions}
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---Apache Arrow---------------------------------------------------
LCGPackage_Add(
  arrow
  URL ${GenURL}/apache-arrow-${arrow_native_version}.tar.gz
  CONFIGURE_COMMAND ${CMAKE_COMMAND} <SOURCE_DIR>/cpp -DARROW_USE_SSE=ON -DARROW_PYTHON=ON -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR> -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
  BUILD_COMMAND make
  INSTALL_COMMAND make install
  DEPENDS Boost Python numpy
)



#---Forward declarations----------------------------------------------------------------------------
#LCGPackage_set_home(numpy)

LCGPackage_set_home(pythia6)
#---ROOT--------------------------------------------------------------------------------------------
string(REGEX MATCH "[0-9]+[.][0-9]+[.][0-9]+" ROOT_author_version "${ROOT_native_version}")

LCGPackage_Add(
    ROOT
    IF <VERSION> MATCHES "^v.*-patches|HEAD" THEN
      GIT_REPOSITORY http://root.cern.ch/git/root.git GIT_TAG <VERSION>
      UPDATE_COMMAND <VOID>
    ELSE
      URL http://root.cern.ch/download/root_v${ROOT_author_version}.source.tar.gz
    ENDIF
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -Dpython=ON
               IF Python_native_version VERSION_GREATER 3 THEN
                 -Dpython3=ON
                 -Dpython_version=3
               ENDIF
               -Dbuiltin_pcre=ON
               -Dcintex=ON
               IF DEFINED Davix_native_version THEN
                 -Ddavix=ON
               ENDIF
               -Dexceptions=ON
               -Dexplicitlink=ON
               -Dfftw3=ON
               -Dgdml=ON
               -Dgsl_shared=ON
               -Dgviz=ON
               -Dhttp=ON
               -Dkrb5=ON
               -Dgenvector=ON
               IF <VERSION> MATCHES "^v6-|^6[.]" AND NOT LCG_TARGET MATCHES mac AND NOT ${LCG_HOST_ARCH} STREQUAL i686 THEN
                 -Dvc=ON
               ENDIF
               -Dldap=ON
               -Dmathmore=ON
               -Dmemstat=ON
               -Dminuit2=ON
               IF DEFINED mysql_native_version THEN
                 -Dmysql=ON
               ENDIF
               -Dodbc=ON
               -Dopengl=ON
               -Dpgsql=OFF
               -Dqtgsi=ON
               -Dreflex=ON
               IF DEFINED R_native_version THEN
                 -Dr=ON
               ENDIF
               -Droofit=ON
               -Drfio=ON
               -Dssl=ON
               -Dtable=ON
               -Dunuran=ON
               -Dfortran=ON
               -Dxft=ON
               -Dxml=ON
               -Dxrootd=ON
               -Dzlib=ON
               -DCINTMAXSTRUCT=36000
               -DCINTMAXTYPEDEF=36000
               -DCINTLONGLINE=4096
               IF LCG_CPP11 OR LCG_CPP1Y THEN
                   -Dc++11=ON
                   -Dcxx11=ON
               ENDIF
               IF LCG_CPP14 THEN
                   -Dc++14=ON
                   -Dcxx14=ON
               ENDIF
               IF LCG_CPP17 THEN
                   -Dc++17=ON
                   -Dcxx17=ON
               ENDIF

               IF LCG_TARGET MATCHES x86_64-slc OR LCG_TARGET MATCHES x86_64-centos THEN
                   -Dcastor=ON
                   -Ddcache=ON
                   -Dgfal=ON -DGFAL_DIR=${gfal_home}
                             -DSRM_IFCE_DIR=${srm_ifce_home}
               ENDIF
               IF DEFINED oracle_native_version AND (LCG_TARGET MATCHES slc OR LCG_TARGET MATCHES centos) THEN
                   -Doracle=ON -DORACLE_HOME=${oracle_home}
               ENDIF
               -Dpythia6=ON
    DEPENDS Python fftw graphviz GSL xrootd numpy tbb blas pythia6 zlib libxml2
            IF DEFINED mysql_native_version THEN
                mysql
            ENDIF
            IF DEFINED Davix_native_version THEN
                Davix
            ENDIF
            IF DEFINED R_native_version THEN
                R
            ENDIF
            IF LCG_TARGET MATCHES x86_64-slc OR LCG_TARGET MATCHES x86_64-centos THEN
                CASTOR dcap gfal srm_ifce
            ENDIF
            IF DEFINED oracle_native_version AND (LCG_TARGET MATCHES slc OR LCG_TARGET MATCHES centos AND NOT LCG_TARGET MATCHES "aarch64") THEN
                oracle
            ENDIF
            #IF LCG_TARGET MATCHES slc OR LCG_TARGET MATCHES centos THEN
            #    Qt
            #ENDIF
            IF <VERSION> MATCHES "^v6-|^6[.]" AND NOT LCG_TARGET MATCHES mac AND NOT ${LCG_HOST_ARCH} STREQUAL i686 THEN
                Vc
            ENDIF
)
# Add extra ROOT external dependencies (tools, and optional components used to test ROOT)
if(NOT APPLE)
  set(extradeps hadoop Qt)
endif()
foreach(dep postgresql openldap libaio gdb cfitsio pythia8 pythia6 R gridexternals
    pytools pango lcgenv libiodbc ninja CMake veccore vdt sas jupyter tbb valgrind ccache
    gcovr arrow libxml2 blas freetype GSL numpy pytest xrootd ${extradeps})
  if(DEFINED ${dep}_native_version OR TARGET ${dep})
    add_dependencies(ROOT-dependencies ${dep})
  endif()
endforeach()

#---RELAX---------------------------------------------------------------------------------------------
LCGPackage_Add(
  RELAX
  URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/RELAX-${RELAX_native_version}.tar.gz
#  SVN_REPOSITORY http://svn.cern.ch/guest/relax/tags/${RELAX_native_version}/relax --quiet
  UPDATE_COMMAND <VOID>
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_MODULE_PATH=${CMAKETOOLS_MODULES}
             -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
             -DROOTSYS=${ROOT_home}
             IF DEFINED GCCXML_native_version THEN
               -DGCCXML=${GCCXML_home}/bin/gccxml
             ENDIF
             -DCLHEP_ROOT_DIR=${CLHEP_home}
             -DHEPMC_ROOT_DIR=${HepMC_home}
             -DGSL_ROOT_DIR=${GSL_home}
             -DHEPPDT_ROOT_DIR=${HepPDT_home}
  BUILD_COMMAND ${MAKE} ROOTSYS=${ROOT_home}
  DEPENDS cmaketools ROOT
          IF DEFINED GCCXML_native_version THEN
            GCCXML
          ENDIF
          CLHEP HepMC HepPDT GSL
)

#---CMT-----------------------------------------------------------------------------------------------
LCGPackage_Add(
  cmt
  URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/CMT-${cmt_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>/CMT
          COMMAND ${CMAKE_COMMAND} -E chdir <INSTALL_DIR>/CMT/${cmt_native_version}/mgr ./INSTALL
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---LCGCMT--------------------------------------------------------------------------------------------
LCGPackage_Add(
  LCGCMT
  URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/LCGCMT-cmake_root6.tar.gz
  UPDATE_COMMAND ${PYTHON} ${CMAKE_SOURCE_DIR}/cmake/scripts/create_LCGCMT.py ${CMAKE_BINARY_DIR}/dependencies.json <SOURCE_DIR>/LCG_Configuration/cmt/requirements
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>/..
  DEPENDS Python
)

# environment variables needed for CMT based builds
#set( LCGCMT_pos "/afs/cern.ch/sw/lcg/app/releases/LCGCMT/LCGCMT_66")
set(LCGCMT_pos "${LCGCMT_home}/.." )
set(CMTROOT ${cmt_home}/CMT/${cmt_native_version})

set(CMT_make_cmd
    ${env_cmd} CMTSITE=CERN CMTCONFIG=${LCG_system} CMTPATH=<SOURCE_DIR>:${LCGCMT_pos}
               CMTROOT=${CMTROOT} CMTBIN=${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}
	       IF LCG_CPP11 THEN -DCMAKE_CXX_FLAGS=-std=c++11 ENDIF
	       IF LCG_CPP14 THEN -DCMAKE_CXX_FLAGS=-std=c++14 ENDIF
	       IF LCG_CPP17 THEN -DCMAKE_CXX_FLAGS=-std=c++17 ENDIF
               ${MAKE} -j1 -f ${CMAKE_SOURCE_DIR}/cmake/utils/Makefile-cmt.mk
                       -C <SOURCE_DIR>
                       CMT=${CMTROOT}/${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}/cmt)

#---CORAL----------------------------------------------------------------------------------------------
set(POST_INSTALL_old ${POST_INSTALL})
set(POST_INSTALL OFF)
LCGPackage_add(
  CORAL
  IF CORAL_native_version MATCHES HEAD THEN
    ###SVN_REPOSITORY http://svn.cern.ch/guest/lcgcoral/coral/trunk --quiet
    GIT_REPOSITORY https://gitlab.cern.ch/lcgcoral/coral.git
    UPDATE_COMMAND <VOID>
  ELSE
    ###URL http://service-spi.web.cern.ch/service-spi/external/tarFiles/CORAL-${CORAL_native_version}.tar.gz # OLD
    URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/CORAL-${CORAL_native_version}.tar.gz
  ENDIF
  CMAKE_GENERATOR Ninja
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
             -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_VERBOSE_MAKEFILE=ON
             -DBINARY_TAG=${BINARY_TAG}
             -DLCG_VERSION=${LCG_VERSION}
             -DLCG_releases_base=${CMAKE_INSTALL_PREFIX}
             ${Boost_extra_configuration}
             IF Python_native_version VERSION_GREATER 3 THEN
               -DLCG_python3=on
               -DPython_ADDITIONAL_VERSIONS=${Python_config_version_twodigit} # needed?
               -DPython_config_version_twodigit=${Python_config_version_twodigit} # needed?
             ENDIF
             ###-DCMAKE_USE_CCACHE=ON # Can also set it via environment variable
  DEPENDS ninja ccache Boost CppUnit expat Frontier_Client libaio mysql Python QMtest XercesC sqlite
  IF NOT BINARY_TAG MATCHES "mac" AND NOT BINARY_TAG MATCHES "clang" THEN
    gperftools
  ENDIF
  IF NOT BINARY_TAG MATCHES "mac" AND NOT BINARY_TAG MATCHES "clang" AND NOT BINARY_TAG MATCHES "gcc62" AND NOT BINARY_TAG MATCHES "gcc7" AND NOT LCG_TARGET MATCHES "aarch64" THEN
    igprof libunwind
  ENDIF
  IF NOT BINARY_TAG MATCHES "mac" AND NOT BINARY_TAG MATCHES "icc" THEN
     valgrind
  ENDIF
  IF NOT LCG_TARGET MATCHES "aarch64" THEN
     oracle
  ENDIF
)
set(POST_INSTALL ${POST_INSTALL_old})
unset(POST_INSTALL_old)

if (NOT CORAL_lcg_exists AND CORAL_native_version AND Python_native_version VERSION_LESS 3)
LCG_add_test(coral-tests
  ENVIRONMENT BINARY_TAG=${BINARY_TAG}
  TEST_COMMAND ${CORAL_home}/run_nightly_tests_cmake.sh
  LABELS Nightly Release
)
endif()

#---COOL-----------------------------------------------------------------------------------------------
set(POST_INSTALL_old ${POST_INSTALL})
set(POST_INSTALL OFF)
LCGPackage_add(
  COOL
  IF COOL_native_version MATCHES HEAD THEN
    ###SVN_REPOSITORY http://svn.cern.ch/guest/lcgcool/cool/trunk --quiet
    SVN_REPOSITORY svn+ssh://svn.cern.ch/reps/lcgcool/cool/trunk --quiet
  ELSE
    ###URL http://service-spi.web.cern.ch/service-spi/external/tarFiles/COOL-${COOL_native_version}.tar.gz # OLD
    URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/COOL-${COOL_native_version}.tar.gz
  ENDIF
  ENVIRONMENT QT_PLUGIN_PATH=${Qt5_home}/plugins
  CMAKE_GENERATOR Ninja
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
             -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_VERBOSE_MAKEFILE=ON
             -DBINARY_TAG=${BINARY_TAG}
             -DLCG_VERSION=${LCG_VERSION}
             -DLCG_releases_base=${CMAKE_INSTALL_PREFIX}
             ${Boost_extra_configuration}
             IF Python_native_version VERSION_GREATER 3 THEN
               -DLCG_python3=on
               -DPython_ADDITIONAL_VERSIONS=${Python_config_version_twodigit} # needed?
               -DPython_config_version_twodigit=${Python_config_version_twodigit} # needed?
             ENDIF
             ###-DCMAKE_USE_CCACHE=ON # Can also set it via environment variable
  DEPENDS CORAL ROOT
  IF NOT BINARY_TAG MATCHES "mac" AND NOT BINARY_TAG MATCHES "icc15" THEN
    Qt5
  ENDIF
)
set(POST_INSTALL ${POST_INSTALL_old})
unset(POST_INSTALL_old)

if (NOT COOL_lcg_exists AND COOL_native_version AND Python_native_version VERSION_LESS 3)
LCG_add_test(cool-tests
  ENVIRONMENT BINARY_TAG=${BINARY_TAG}
  TEST_COMMAND ${COOL_home}/run_nightly_tests_cmake.sh
  LABELS Nightly Release
)
endif()

#---GaudiDeps------------------------------------------------------------------------------------------
#set(GAUDI-dependencies AIDA Boost Python GSL ROOT QMtest CLHEP HepMC HepPDT RELAX
#                       GCCXML tbb XercesC uuid LCGCMT CppUnit gperftools)
set(GAUDI-alldeps  AIDA Boost Python GSL ROOT COOL CORAL QMtest CLHEP HepMC HepPDT RELAX
                   uuid tbb XercesC LCGCMT CppUnit gperftools GCCXML pytools pygraphics
                   ninja CMake)

set(GAUDI-dependencies)
foreach(dep ${GAUDI-alldeps})
  if(DEFINED ${dep}_native_version)
    set(GAUDI-dependencies ${GAUDI-dependencies} ${dep})
  endif()
endforeach()

foreach(dep ${GAUDI-dependencies})
  if(TARGET install-${dep})
    list(APPEND GAUDI-installs install-${dep})
  endif()
endforeach()

add_custom_target(GAUDI-externals
                 COMMENT "Target to build all externals packages needed by Gaudi"
                 DEPENDS ${GAUDI-dependencies})
#---GeantVDeps-----------------------------------------------------------------------------------------
set(GeantV-alldeps ROOT VecGeom Python Boost fftw graphviz GSL mysql xrootd R numpy tbb blas pythia6
                   Geant4 XercesC CLHEP expat hepmc3 umesimd VecGeom Vc veccore veccorelib benchmark ninja CMake lcgenv)
foreach(dep ${GeantV-alldeps})
  if(DEFINED ${dep}_native_version)
    set(GeantV-dependencies ${GeantV-dependencies} ${dep})
  endif()
endforeach()
add_custom_target(GeantV-externals
   COMMENT "Target to build all externals packages needed by GeantV"
   DEPENDS ${GeantV-dependencies} )

#---Geant4Deps-----------------------------------------------------------------------------------------
set(Geant4-alldeps ROOT VecGeom Python Qt5 tbb XercesC CLHEP expat ninja ccache CMake lcgenv)
foreach(dep ${Geant4-alldeps})
  if(DEFINED ${dep}_native_version)
    set(Geant4-dependencies ${Geant4-dependencies} ${dep})
  endif()
endforeach()
add_custom_target(Geant4-externals
   COMMENT "Target to build all externals packages needed by Geant4"
   DEPENDS ${Geant4-dependencies} )

#---LCIO-----------------------------------------------------------------------------------------------
LCGPackage_add(
  LCIO
  SVN_REPOSITORY svn://svn.freehep.org/lcio/tags/<LCIO_native_version> --quiet
  UPDATE_COMMAND <VOID>
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DLCIO_GENERATE_HEADERS=off
             -DBUILD_ROOTDICT=ON
             -DROOT_DIR=${ROOT_home}
             IF LCG_CPP11 THEN
             -DCMAKE_CXX_FLAGS=-std=c++11
             ENDIF
             IF LCG_CPP1Y THEN
             -DCMAKE_CXX_FLAGS=-std=c++1y
             ENDIF
             IF LCG_CPP14 THEN
             -DCMAKE_CXX_FLAGS=-std=c++14
             ENDIF
             IF LCG_CPP17 THEN
	     -DCMAKE_CXX_FLAGS=-std=c++17
             ENDIF
  BUILD_COMMAND ${MAKE} ROOTSYS=${ROOT_home}
  DEPENDS ROOT
)

#---VecGeom------------------------------------------------------------------------------------------
LCGPackage_add(
  VecGeom
  IF VecGeom_native_version STREQUAL HEAD THEN
    GIT_REPOSITORY https://gitlab.cern.ch/VecGeom/VecGeom.git
    UPDATE_COMMAND <VOID>
  ELSE 
    URL ${GenURL}/VecGeom-${VecGeom_native_version}.tar.gz
  ENDIF
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DBACKEND=Vc
             -DVc=ON
             -DGEANT4=OFF
             -DUSOLIDS=ON
             -DUSOLIDS_VECGEOM=ON
             -DROOT=ON
             -DCTEST=OFF
             -DBUILTIN_VECCORE=OFF
             -DBUILD_SHARED_LIBS=ON
              IF LCG_INSTRUCTIONSET THEN 
                -DVECGEOM_VECTOR=${LCG_INSTRUCTIONSET}
              ELSE
                -DVECGEOM_VECTOR=sse4.2
              ENDIF
             -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_WRAPPER}
  BUILD_WITH_INSTRUCTION_SET 1
  DEPENDS Vc veccore ROOT
  REVISION 3
)

#---Geant4---------------------------------------------------------------------------------------------
LCGPackage_add(
  Geant4
#  URL http://geant4.cern.ch/support/source/geant4.${Geant4_native_version}.tar.gz
  URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/geant4.${Geant4_native_version}.tar.gz
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DGEANT4_USE_GDML=ON
             -DXERCESC_ROOT_DIR=${XercesC_home}
             -DGEANT4_USE_SYSTEM_CLHEP=ON
             -DGEANT4_USE_G3TOG4=ON
             IF DEFINED Qt5_native_version THEN
               -DGEANT4_USE_QT=ON
               -DGEANT4_USE_OPENGL_X11=ON
               -DGEANT4_USE_XM=ON
               -DGEANT4_USE_RAYTRACER_X11=ON
               -DGEANT4_BUILD_MULTITHREADED=ON
             ENDIF
             -DQT_QMAKE_EXECUTABLE=${Qt_home}/bin/qmake
             -DGEANT4_INSTALL_DATA=ON
             -DGEANT4_BUILD_TLS_MODEL=global-dynamic
             -DGEANT4_USE_SYSTEM_EXPAT=OFF
             IF <VERSION> VERSION_LESS 10.04 THEN
               -DGEANT4_USE_USOLIDS=ON
               -DUSolids_DIR=${VecGeom_home}/lib/cmake/USolids/
             ELSE
               -DGEANT4_USE_USOLIDS=ALL
               -DVecGeom_DIR=${VecGeom_home}/lib/cmake/VecGeom
             ENDIF
             IF LCG_CPP11 THEN  -DGEANT4_BUILD_CXXSTD=c++11  ENDIF
             IF LCG_CPP1Y THEN  -DGEANT4_BUILD_CXXSTD=c++14  ENDIF  # c++1y is not supported by Geant4
             IF LCG_CPP14 THEN  -DGEANT4_BUILD_CXXSTD=c++14  ENDIF
	     IF LCG_CPP17 THEN  -DGEANT4_BUILD_CXXSTD=c++17  ENDIF
  BUILD_COMMAND ${MAKE}
  INSTALL_COMMAND make install
  DEPENDS IF DEFINED Qt5_native_version THEN Qt5 ENDIF XercesC CLHEP VecGeom expat
  REVISION 1
)

#---DD4hep-----------------------------------------------------------------------------------------------
LCGPackage_add(
  DD4hep
  URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/DD4hep-${DD4hep_native_version}.tar.gz
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DBOOST_ROOT=${Boost_home}
             IF LCG_CPP11 THEN  -DDD4HEP_USE_CXX11=ON  ENDIF
             IF LCG_CPP1Y THEN  -DDD4HEP_USE_CXX14=ON  ENDIF  # c++1y is not supported by DD4hep
             IF LCG_CPP14 THEN  -DDD4HEP_USE_CXX14=ON  ENDIF
	     IF LCG_CPP17 THEN  -DDD4HEP_USE_CXX17=ON  ENDIF
             -DDD4HEP_USE_XERCESC=ON
             -DXERCESC_ROOT_DIR=${XercesC_home}
             -DROOTSYS=${ROOT_home}
             -DDD4HEP_USE_GEANT4=ON
             ${Boost_extra_configuration}
  DEPENDS ROOT XercesC Geant4 Boost
)

#---delphes-----------------------------------------------------------------------------------------------
LCGPackage_add(
  delphes
  URL ${GenURL}/Delphes-${delphes_native_version}.tar.gz
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
             -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
  DEPENDS ROOT
)

#---hepmc3----------------------------------------------------------------------------
if( hepmc3_native_version MATCHES "githead")
  set(hepmc3_source_command GIT_REPOSITORY https://gitlab.cern.ch/hepmc/HepMC3.git
                            UPDATE_COMMAND <VOID> )
else()
  set(hepmc3_source_command URL ${GenURL}/HepMC3-${hepmc3_native_version}.tar.gz)
endif()

LCGPackage_set_home(pythia8)  # forward declaration  (citcular dependency?)

LCGPackage_Add(
  hepmc3
  ${hepmc3_source_command}
  ENVIRONMENT ROOTSYS=${ROOT_home}
  CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DPYTHIA8_ROOT_DIR=${pythia8_home}
	           -DROOT_DIR=${ROOT_home}
	           -DHEPMC_BUILD_EXAMPLES=ON
  BUILD_COMMAND ${MAKE} -j1
  DEPENDS pythia8 ROOT
)
